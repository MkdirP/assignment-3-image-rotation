#ifndef IMAGE_ROTATION_ROTATE_H
#define IMAGE_ROTATION_ROTATE_H

#include "image.h"

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct image const source );

#endif

#ifndef IMAGE_ROTATION_BMP_IMAGE_H
#define IMAGE_ROTATION_BMP_IMAGE_H

#include <stdint.h>
#include <stdbool.h>
#include <inttypes.h>
#include <stdlib.h>

struct pixel { uint8_t b, g, r; };

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct image bmp_image_malloc(uint32_t width, uint32_t height);

void bmp_image_clear(struct image* image);

#endif

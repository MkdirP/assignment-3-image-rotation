#ifndef IMAGE_ROTATION_FILE_H
#define IMAGE_ROTATION_FILE_H

#include <stdio.h>
#include <stdbool.h>

bool comm_file_open (const char* path, FILE** file, const char* mode);

bool comm_file_close (FILE* file);

#endif 

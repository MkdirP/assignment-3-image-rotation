#include <stdio.h>
#include <sysexits.h>
// EX_OK           0       /* successful termination */
// EX__BASE        64      /* base value for error messages */
// EX_USAGE        64      /* command line usage error */
// EX_DATAERR      65      /* data format error */
// EX_NOINPUT      66      /* cannot open input */    
// EX_NOUSER       67      /* addressee unknown */    
// EX_NOHOST       68      /* host name unknown */
// EX_UNAVAILABLE  69      /* service unavailable */
// EX_SOFTWARE     70      /* internal software error */
// EX_OSERR        71      /* system error (e.g., can't fork) */
// EX_OSFILE       72      /* critical OS file missing */
// EX_CANTCREAT    73      /* can't create (user) output file */
// EX_IOERR        74      /* input/output error */
// EX_TEMPFAIL     75      /* temp failure; user is invited to retry */
// EX_PROTOCOL     76      /* remote error in protocol */
// EX_NOPERM       77      /* permission denied */
// EX_CONFIG       78      /* configuration error */

#include "bmp.h"
#include "comm_file.h"
#include "bmp_image.h"
#include "rotation.h"

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    if (argc != 3) {
        fprintf(stderr, "The program takes 2 parameters, which you specify %d\n", argc - 1);
        return EX_USAGE;
    }
    FILE* in = NULL;
    if (!comm_opening_file(argv[1], &in, "rb")){
        fprintf(stderr, "Failed to open file '%s'\n", argv[1]);
        return EX_NOINPUT;
    }
    struct image img = {0};
    switch (from_bmp(in, &img)) {
        case READ_INVALID_BITS:
            fprintf(stderr, "Invalid bits\n");
            comm_closing_file(in);
            return EX_DATAERR;
        case READ_INVALID_SIGNATURE:
            fprintf(stderr, "Invalid signature\n");
            comm_closing_file(in);
            return EX_DATAERR;
        case READ_INVALID_HEADER:
            fprintf(stderr, "Invalid header\n");
            comm_closing_file(in);
            return EX_DATAERR;
        case READ_FROM_NULL_FILE:
            fprintf(stderr, "NUll file, Failed to open file '%s'\n", argv[1]);
            comm_closing_file(in);
            return EX_NOINPUT;
        case READ_OK:
            break;
    }
    if(!comm_closing_file(in)) {
        fprintf(stderr, "Closing file Error '%s'\n", argv[1]);
        bmp_image_clear(&img);
        return EX_IOERR;
    }
    struct image new_img = rotate(img);
    bmp_image_clear(&img);
    FILE* out = NULL;
    if(!comm_opening_file(argv[2], &out, "wb")) {
        fprintf(stderr, "Could not create file '%s' or access is denied\n", argv[2]);
        bmp_image_clear(&new_img);
        return EX_CANTCREAT;
    }
    if (to_bmp(out, &new_img)) {
        fprintf(stderr, "Write bmp file Error\n");
        bmp_image_clear(&new_img);
        comm_closing_file(out);
        return EX_IOERR;
    }
    bmp_image_clear(&new_img);
    if (!comm_closing_file(out)) {
        fprintf(stderr, "Closing file Error '%s'\n", argv[2]);
        return EX_IOERR;
    }
    return EX_OK;
}

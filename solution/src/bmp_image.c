#include <stdlib.h>
#include "bmp_image.h"

// struct image {
//   uint64_t width, height;
//   struct pixel* data;
// };

// struct pixel { uint8_t b, g, r; };

struct image bmp_image_malloc(uint32_t width, uint32_t height){
    struct image image = {
        .width = width,
        .height = height,
        .data = malloc(width * height *sizeof(struct pixel))
    };
    return image;
}

void bmp_image_clear(struct image* image){
    free(image->data);
}



#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "bmp_image.h"
#include "bmp.h"


struct bmp_header __attribute__((packed))
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};

// +
uint64_t get_padding(uint64_t width) {
    if ((width * 3) % 4) {
        return 4 - ((width * 3) % 4);
    } else {
        return 0;
    }
}

// +
bool set_padding(FILE* out, uint64_t width) {
    int8_t un = 0;
    for (size_t i = 0; i < get_padding(width); i++){
        if(!fwrite(&un, sizeof (int8_t), 1, out)) return false;
    }
    return true;
}

// 12 пикселей = 12 * 3 байт = 36 байт. 
// 5 пикселей. 5 * 3 = 15 байт.
bool get_bmp_image ( FILE* in, const struct bmp_header header, struct image* img) {
    if (header.biHeight <= 0 || header.biWidth <= 0) return false;
    *img = bmp_image_malloc(header.biWidth, header.biHeight);
    fseek(in, header.bOffBits, SEEK_SET);
    for (size_t i = 0; i < img->height; i++) {
        for (size_t k = 0; k < img->width; k++) {
            struct pixel pixel = {0};
            if (!fread(&pixel, sizeof (struct pixel), 1, in)) {
                bmp_image_clear(img);
                return false;
            }
            if (k < img->width && i < img->height) {
                img->data[i * img->width + k] = pixel;
            }
        }
        if ((img->width * 3) % 4) fseek(in, get_padding(img->width), SEEK_CUR);
    }
    return true;
}

//+
struct bmp_header create_header(const struct image* image) {
    struct bmp_header header = {
        .bfType = 0x4D42,
        .bfileSize = sizeof (struct bmp_header)
                + image->width * image->height * sizeof(struct pixel)
                + image->height * get_padding(image->width),
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biWidth = image->width,
        .biHeight = image->height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
    header.biSizeImage = header.bfileSize - header.bOffBits;
    return header;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    if (in == NULL) return READ_FROM_NULL_FILE;
    struct bmp_header header;
    if (!(header.bfType == 0x4D42)) return READ_INVALID_SIGNATURE;
    if (!fread(&header, sizeof(struct bmp_header), 1, in)) return READ_INVALID_HEADER;
    if (!get_bmp_image(in, header, img)) return READ_INVALID_BITS;
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header header = create_header(img);
    if (!fwrite(&header, sizeof (struct bmp_header), 1, out)) return WRITE_ERROR;
    for (size_t i = 0; i < img->height; i++){
        for (size_t k = 0; k < img->width; k++) {
            struct pixel pxl = img->data[k * img->width + i];
            if (!fwrite(&pxl, sizeof (struct pixel), 1, out)) return WRITE_ERROR;
        }
        if(!set_padding(out, img->width)) return WRITE_ERROR;
    }
    return WRITE_OK;
}

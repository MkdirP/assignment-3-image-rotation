#include <stdio.h>
#include "comm_file.h"

bool comm_opening_file (const char* path, FILE** file, const char* mode){
    if (!path || !*file) return 0;
    *file = fopen(path, mode);
    return 1;
}

bool comm_closing_file (FILE* file){
    if (!file || fclose(file)) return 0;
    return 1;
}

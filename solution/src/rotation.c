#include "rotation.h"
#include "bmp_image.h"

struct image rotate( struct image const source ) {
    struct image img = bmp_image_malloc(source.height, source.width);
    for (uint64_t i = 0; i < source.height; i++) {
        for (uint64_t k = 0; k < source.width; k++) {
            img.data[i+((img.height-k-1) * img.width)] = source.data[k+i * source.width];
        }
    }
    return img;
}
